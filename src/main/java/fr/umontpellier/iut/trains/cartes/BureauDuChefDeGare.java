package fr.umontpellier.iut.trains.cartes;

public class BureauDuChefDeGare extends Carte {
    public BureauDuChefDeGare() {
        super("Bureau du chef de gare");
    }
}
