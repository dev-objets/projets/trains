package fr.umontpellier.iut.trains.cartes;

public class AtelierDeMaintenance extends Carte {
    public AtelierDeMaintenance() {
        super("Atelier de maintenance");
    }
}
